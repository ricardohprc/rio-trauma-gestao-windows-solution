﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ECN.RTG_Automation.Behavior;
using ECN.RTG_Automation.Helpers;

namespace ECN.RTG_Automation
{
    public partial class SingleEmail : Form
    {

        private string PDF;
        private PdfUserClass Person;

        public SingleEmail(PdfUserClass person)
        {
            InitializeComponent();
            Settings settings = Settings.ReadGarageXML();
            this.Person = person;
            NomeBox.Text = person.Nome;
            EmailBox.Text = person.Email;
            unidadeBox.Text = person.Unidade;
            mesAtualPicker.Value = person.MesAtual;
            mesVigenciaPicker.Value = person.MesVigencia;
            PreviewBox.Text = person.ReturnFormatedString(settings.MessagePattern);
            PDF = person.NomePdf;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveBusinessValueClass();
            AsyncProcessBehavior.instance.SendEmailTo(Person);
        }

        public void SaveBusinessValueClass()
        {
            int index = Form1.instance.businessList.IndexOf(Person);
            Person.Nome = NomeBox.Text;
            Person.Email = EmailBox.Text;
            Person.Unidade = unidadeBox.Text;
            Person.MesAtual = mesAtualPicker.Value;
            Person.MesVigencia = mesVigenciaPicker.Value;
            Form1.instance.businessList[index] = Person;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveBusinessValueClass();
            Person.UpdateInformation();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Person.NomePdf);
        }
    }
}
