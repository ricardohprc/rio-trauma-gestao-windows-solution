﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ECN.RTG_Automation.Helpers
{
    [Serializable]
    public class Settings
    {

        public int SMTPPort;
        public string SMTPUser;
        public string SMTPPassword;
        public string SMTPServer;
        public string[] MessagePattern;
        public string EmailSubject;
        private static string _StartupPath = System.Reflection.Assembly.GetEntryAssembly().Location;
        private static string _StartupFile = "/../config.xml";
        private static string _StartupXML = _StartupPath + _StartupFile;


        public static void WriteGarageXML(Settings pInstance)
        {
            XmlSerializer writer = new XmlSerializer(typeof(Settings));
            using (FileStream file = File.OpenWrite(_StartupXML))
            {
                writer.Serialize(file, pInstance);
            }
        }
        public static Settings ReadGarageXML()
        {
            XmlSerializer reader = new XmlSerializer(typeof(Settings));
            if (!File.Exists(_StartupXML))
            {
                WriteGarageXML(new Settings());
            }
            using (FileStream input = File.OpenRead(_StartupXML))
            {
                return reader.Deserialize(input) as Settings;
            }
        }

    }
}
