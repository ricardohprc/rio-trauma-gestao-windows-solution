﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ECN.RTG_Automation.Helpers
{
    public class BusinessValueClass
    {
        public string Nome { get; set; }
        public string Unidade { get; set; }
        public string NomePdf { get; set; }
        public DateTime MesAtual { get; set; }
        public DateTime MesVigencia { get; set; }
        public string Email { get; set; }

        private const string UNIDADEPATTERN = "UNIDADE:";
        private const string RAZAOSOCIALPATTERN = "CNPJ:";
        private const string NOMEPATTERN = "NOME:";
        private const string EMAILPATTERN = "E-MAIL:";
        private const string CPFPATTERN = "CPF:";

        public static BusinessValueClass instance;
        private static string[] defaults = new string[] {"Sem Unidade", "Nome não encontrado", "sem@email.com", DateTime.Today.ToString()};
        private static string[] Months = new string[] { "JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL",
        "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"};
        private static string[] MonthsPresentation = new string[] { "Janeiro", "Fevereiro", "Março", "Abril",
            "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};

        public BusinessValueClass(string pdfString, string pdfPath)
        {
            pdfString = pdfString.Replace("\n", "");
            string[] patterns = pdfString.Split(new string[] { UNIDADEPATTERN, RAZAOSOCIALPATTERN, NOMEPATTERN, EMAILPATTERN, CPFPATTERN, Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            this.Unidade = TryToGetString("Unidade",patterns, 1);
            this.Nome = TryToGetString("Nome", patterns, 3);
            this.Email = TryToGetString("Email", patterns, 4);
            SetDateTime(TryToGetString("Data", patterns, 2));
            this.NomePdf = pdfPath;
            instance = this;
        }

        private string TryToGetString(string alias, string[] array, int i)
        {
            try
            {
                return array[i];
            }
            catch (Exception e)
            {
                SetAsFailedUser();
                return "";
            }
        }

        public void SetDateTime(string str)
        {
            try
            {
                string[] data = str.Split('/');
                int month = Months.ToList().IndexOf(data[0].ToUpper().Trim());
                int monthVigencia = month - 1;
                int year = Int32.Parse(data[1].Trim());
                int yearvigencia = year;
                if (month == 1)
                {
                    monthVigencia = 12;
                    yearvigencia = year - 1;
                }

                MesVigencia = new DateTime(yearvigencia, monthVigencia, 1);
                MesAtual = new DateTime(year, month, 1);
                SetSuccessUser();
            }
            catch (Exception e)
            {
                this.MesAtual = DateTime.Now;
                this.MesVigencia = DateTime.Now;
                SetAsFailedUser();
            }
        }

        internal void SetAsFailedUser()
        {
            Form1.instance.SetFailEmail(this);

        }

        internal void SetSuccessUser()
        {
            Form1.instance.SetSuccessEmail(this);

        }

        public string GetDateTime(DateTime dateTime)
        {
            string month = MonthsPresentation[dateTime.Month];
            return month + "/" + dateTime.Year;
        }

        public string ReturnFormatedString(string[] rawStringArray)
        {
            string unformatedString = ArrayToString(rawStringArray);
            string formatedString = FindFormatedPattern(unformatedString);
            formatedString = FindBoldPattern(formatedString);
            return formatedString;

        }
        /*[] 0 1 -> 0
        [] * [] 0 2 -> 0
        [] * [] * [] 2 3 -> 2
        [] * [] * [] * [] 2 4 -> 2
        [] * [] * [] * [] * [] 4 5 -> 4
        [] * [] * [] * [] * [] * [] 4 6 -> 4
        [] * [] * [] * [] * [] * [] * [] 6 7 -> 6*/
        public string FindBoldPattern(string unformatedString)
        {
            int j = 0;
            string formatedString = "";
            string[] words = unformatedString.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries);
            int amountOfReplacements = 0;
            if (words.Length >= 3)
            {
                amountOfReplacements = words.Length % 2 == 0 ? words.Length - 2 : words.Length - 1;
                amountOfReplacements = amountOfReplacements / 2;
            }
            for (int i = 0; i < words.Length; i++)
            {
                if (i % 2 != 0 && amountOfReplacements != 0)
                {
                    formatedString += "<b>" + words[i] + "</b>";
                    amountOfReplacements--;
                }
                else
                {
                    formatedString += words[i];
                }
            }

            return formatedString;
        }

        public string FindFormatedPattern(string unformatedString)
        {
            string formatedString = "";
            string[] words = unformatedString.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string w in words)
            {
                formatedString += ReturnFormatedValue(w) + " ";
            }

            return formatedString;
        }

        public string ReturnFormatedString(string unformatedString)
        {
            string formatedString = "";
            string[] words = unformatedString.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string w in words)
            {
                formatedString += ReturnFormatedValue(w) + " ";
            }
            return formatedString;
        }

        private string ArrayToString(string[] rawStringArray)
        {
            string output = "";
            foreach (string str in rawStringArray)
            {
                output += str + "\r\n";
            }

            return output;
        }

        public string ReturnFormatedValue(string pattern)
        {
            pattern = pattern.Replace("{Nome}", Nome);
            pattern = pattern.Replace("{Unidade}", Unidade);
            pattern = pattern.Replace("{MesVigencia}", GetDateTime(MesVigencia));
            pattern = pattern.Replace("{MesAtual}", GetDateTime(MesAtual));
            pattern = pattern.Replace("{Email}", Email);
            return pattern;
        }

        public void UpdateInformation()
        {
            CheckForAllInformation();
        }
        
        private void CheckForAllInformation()
        {
            if (Nome.Length == 0 ||
                NomePdf.Length == 0 ||
                Email.Length == 0 ||
                Unidade.Length == 0 ||
                MesAtual.Equals(DateTime.MinValue) ||
                MesVigencia.Equals(DateTime.MinValue))
            {
                SetAsFailedUser();
            }
            else
            {
                SetSuccessUser();
            }
        }
    }
}
