﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using ECN.RTG_Automation.Controller;
using ECN.RTG_Automation.Helpers;


namespace ECN.RTG_Automation.Behavior
{
    public class AsyncProcessBehavior
    {
        PdfToTextController autoTest01;
        ZipExtractorController autoTest02;
        SendEmailController autoTeste03;
        Settings settings;
        public bool isDoneZip { get; set; }
        public bool isDoneEmail { get; set; }
        Form1 form1Instance;
        public static AsyncProcessBehavior instance;
        public Thread[] PDFReader;
        public bool[] ThreadsControl;
        //private long[] ThreadsTime;

        private const int ThreadNumber = 5;

        public AsyncProcessBehavior(Form1 form)
        {
            instance = this;
            settings = Settings.ReadGarageXML();
            autoTest01 = new PdfToTextController();
            autoTest02 = new ZipExtractorController();
            autoTeste03 = new SendEmailController();
            form1Instance = form;
            form1Instance.FillForm(settings);
            isDoneZip = false;
            isDoneEmail = false;
            PDFReader = new Thread[ThreadNumber];
            ThreadsControl = new bool[ThreadNumber];
            //ThreadsTime = new long[ThreadNumber];
        }

        public void Log(string t)
        {
            form1Instance.log(t);
        }

        public void ProcessZio(string path)
        {
            form1Instance.log("Lendo o Zip " + path);
            form1Instance.log("Aguarde...");
            form1Instance.ProgressCounter(0);
            autoTest02.Extract(path, ZioCallback);
        }

        private int FindThread()
        {
            for (int i = 0; i < PDFReader.Length; i++)
            {
                if(PDFReader[i] == Thread.CurrentThread)
                {
                    return i;
                }
            }

            for (int i = 0; i < PDFReader.Length; i++)
            {
                if (PDFReader[i].ThreadState == ThreadState.Stopped)
                {
                    return i;
                }
            }

            return PDFReader.Length - 1;
        }

        internal void FreeThread()
        {
            int i = FindThread();
            ThreadsControl[i] = false;
            /*ThreadsTime[i] = System.DateTime.Now.Ticks - ThreadsTime[i];
            form1Instance.log("Tempo Total para Leitura do PDF: " + ThreadsTime[i] / System.TimeSpan.TicksPerSecond + " segundos" );*/
        }

        public void ProcessPDF(string path)
        {
            form1Instance.log("Lendo PDF " + path);
            autoTest01.Read(path, ReCallback);
            form1Instance.ProgressCounter(100);
        }

        internal void SendEmailTo(string[] peopleString)
        {
            Form1.instance.Loading(false);
            new Thread(()=> {
                List<PdfUserClass> businessValueClass = new List<PdfUserClass>();
                foreach (string str in peopleString)
                {
                    businessValueClass.Add(Form1.instance.FindPersonSameName(str));
                }

                float i = 0;
                foreach (PdfUserClass business in businessValueClass)
                {
                    float progress = i++ / businessValueClass.Count;
                    progress = progress * 100;
                    Form1.instance.ProgressCounter((int)Math.Round(progress));
                    if (progress > 90)
                    {
                        Form1.instance.Loading(true);
                    }
                    try
                    {
                        autoTeste03.SendMail(business);
                        Form1.instance.SuccessEmail(business);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        AsyncProcessBehavior.instance.Log("Erro no envio de email. Favor ver Configuracoes");
                    }
                }
            }).Start();
        }

        internal void SendEmailTo(PdfUserClass business)
        {
                try
                {
                    autoTeste03.SendMail(business);
                    Form1.instance.SuccessEmail(business);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    AsyncProcessBehavior.instance.Log("Erro no envio de email. Favor ver Configuracoes");
                }
        }

        private int GetAvaliableThreadSlot()
        {
            for (int i = 0; i < ThreadsControl.Length; i++)
            {
                if (!ThreadsControl[i])
                {
                    ThreadsControl[i] = true;
                    //ThreadsTime[i] = System.DateTime.Now.Ticks;
                    return i;
                }
            }
            return -1;
        }

        public void ZioCallback(string newPath)
        {
            int threadnum;
            string[] files = Directory.GetFiles(newPath);
            new Thread(() =>
            {
                float i = 0;
                foreach (string f in files)
                {
                    threadnum = GetAvaliableThreadSlot();
                    while (threadnum == -1)
                    {
                        threadnum = GetAvaliableThreadSlot();
                        Thread.Sleep(300);
                    }

                    PDFReader[threadnum] = new Thread(() =>
                    {
                        Thread.CurrentThread.IsBackground = true;
                        form1Instance.AdicionaFila(f);
                        autoTest01.Read(f, ReCallback);
                    });
                    i++;
                    try
                    {
                        PDFReader[threadnum].Start();
                    }
                    catch (Exception e)
                    {
                        continue;
                    }

                    form1Instance.ProgressCounter((int)Math.Round(i / ((float)files.Length) * 100)); 
                }
                Form1.instance.UnlockInterface();
            }).Start();

            string[] directories = Directory.GetDirectories(newPath);
            foreach (string f in directories)
            {
                ZioCallback(f);
            }
        }

        public void ReCallback(string str, string path)
        {
            form1Instance.Invoke(new Action(() => form1Instance.AddToChecklistBox(str, path)));
            AsyncProcessBehavior.instance.FreeThread();
        }
    }
}
