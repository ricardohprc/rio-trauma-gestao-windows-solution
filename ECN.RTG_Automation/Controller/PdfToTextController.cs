﻿using System;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace ECN.RTG_Automation.Controller
{
    public class PdfToTextController
    {
        public static PdfToTextController instance;
        public PdfToTextController()
        {
            instance = this;
        }

		public void Read(string path, Action<string, string> callback)
        {
            PdfReader pdf = new PdfReader(path);
            string Results = PdfTextExtractor.GetTextFromPage(pdf, 1);
            Form1.instance.RemoveFila(path);
            callback.Invoke(Results, path);
        }
    }
}
