﻿using ECN.RTG_Automation.Behavior;
using ECN.RTG_Automation.Helpers;
using System;
using System.Net.Mail;

namespace ECN.RTG_Automation.Controller
{
    public class SendEmailController
    {
        public void SendMail(PdfUserClass emailTo)
        {
            Settings settings = Settings.ReadGarageXML();
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(settings.SMTPServer);
            mail.From = new MailAddress(settings.SMTPUser);
            mail.To.Add(emailTo.Email);
            mail.Subject = PdfUserClass.instance.ReturnFormatedString(settings.EmailSubject);
            mail.Body = emailTo.ReturnFormatedString(settings.MessagePattern);
            mail.IsBodyHtml = true;

            Attachment attachment;
            attachment = new Attachment(emailTo.NomePdf);
            mail.Attachments.Add(attachment);

            SmtpServer.Port = settings.SMTPPort;
            SmtpServer.Credentials = new System.Net.NetworkCredential(settings.SMTPUser, settings.SMTPPassword);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            AsyncProcessBehavior.instance.Log("Email enviado para " + emailTo.Email);

        }
    }
}
