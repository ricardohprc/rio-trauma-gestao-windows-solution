﻿using ECN.RTG_Automation.Behavior;
using System;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace ECN.RTG_Automation.Controller
{
    public class RTGAutoTest01
    {
        public static RTGAutoTest01 instance;
        public bool isFinished { get; private set; }
        public RTGAutoTest01()
        {
            instance = this;
            isFinished = false;
        }

		public void Reset()
        {
            isFinished = false;
        }

		public void Read(string path, Action<string, string> callback)
        {
            isFinished = true;
            //OCR Advanced Longhand
            PdfReader pdf = new PdfReader(path);
            string Results = PdfTextExtractor.GetTextFromPage(pdf, 1);
            isFinished = false;
            Form1.instance.RemoveFila(path);
            callback.Invoke(Results, path);
        }
    }
}
