﻿namespace ECN.RTG_Automation
{
    partial class SingleEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SingleEmail));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.NomeBox = new System.Windows.Forms.TextBox();
            this.EmailBox = new System.Windows.Forms.TextBox();
            this.PreviewBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.unidadeBox = new System.Windows.Forms.TextBox();
            this.UnidadeLabel = new System.Windows.Forms.Label();
            this.mesAtualPicker = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.mesVigenciaPicker = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(483, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(171, 27);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.Location = new System.Drawing.Point(483, 78);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(171, 37);
            this.button2.TabIndex = 1;
            this.button2.Text = "Enviar email";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 246);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Preview";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(545, 301);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(68, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // NomeBox
            // 
            this.NomeBox.Location = new System.Drawing.Point(16, 38);
            this.NomeBox.Name = "NomeBox";
            this.NomeBox.Size = new System.Drawing.Size(461, 22);
            this.NomeBox.TabIndex = 6;
            // 
            // EmailBox
            // 
            this.EmailBox.Location = new System.Drawing.Point(16, 93);
            this.EmailBox.Name = "EmailBox";
            this.EmailBox.Size = new System.Drawing.Size(461, 22);
            this.EmailBox.TabIndex = 7;
            // 
            // PreviewBox
            // 
            this.PreviewBox.Location = new System.Drawing.Point(15, 266);
            this.PreviewBox.Multiline = true;
            this.PreviewBox.Name = "PreviewBox";
            this.PreviewBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.PreviewBox.Size = new System.Drawing.Size(461, 118);
            this.PreviewBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(523, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "PDF Carregado";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(483, 45);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 27);
            this.button3.TabIndex = 10;
            this.button3.Text = "Salvar Dados";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // unidadeBox
            // 
            this.unidadeBox.Location = new System.Drawing.Point(16, 150);
            this.unidadeBox.Name = "unidadeBox";
            this.unidadeBox.Size = new System.Drawing.Size(461, 22);
            this.unidadeBox.TabIndex = 12;
            // 
            // UnidadeLabel
            // 
            this.UnidadeLabel.AutoSize = true;
            this.UnidadeLabel.Location = new System.Drawing.Point(12, 124);
            this.UnidadeLabel.Name = "UnidadeLabel";
            this.UnidadeLabel.Size = new System.Drawing.Size(61, 17);
            this.UnidadeLabel.TabIndex = 11;
            this.UnidadeLabel.Text = "Unidade";
            // 
            // mesAtualPicker
            // 
            this.mesAtualPicker.Location = new System.Drawing.Point(15, 210);
            this.mesAtualPicker.Name = "mesAtualPicker";
            this.mesAtualPicker.Size = new System.Drawing.Size(211, 22);
            this.mesAtualPicker.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Mês Atual";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(266, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Mês Vigência";
            // 
            // mesVigenciaPicker
            // 
            this.mesVigenciaPicker.Location = new System.Drawing.Point(269, 210);
            this.mesVigenciaPicker.Name = "mesVigenciaPicker";
            this.mesVigenciaPicker.Size = new System.Drawing.Size(208, 22);
            this.mesVigenciaPicker.TabIndex = 15;
            // 
            // SingleEmail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 398);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.mesVigenciaPicker);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.mesAtualPicker);
            this.Controls.Add(this.unidadeBox);
            this.Controls.Add(this.UnidadeLabel);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PreviewBox);
            this.Controls.Add(this.EmailBox);
            this.Controls.Add(this.NomeBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "SingleEmail";
            this.Text = "SingleEmail";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox NomeBox;
        private System.Windows.Forms.TextBox EmailBox;
        private System.Windows.Forms.TextBox PreviewBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox unidadeBox;
        private System.Windows.Forms.Label UnidadeLabel;
        private System.Windows.Forms.DateTimePicker mesAtualPicker;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker mesVigenciaPicker;
    }
}