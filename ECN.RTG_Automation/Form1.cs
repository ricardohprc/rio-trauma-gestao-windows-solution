﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using ECN.RTG_Automation.Behavior;
using ECN.RTG_Automation.Helpers;

namespace ECN.RTG_Automation
{
    public partial class Form1 : Form
    {
        private SingleEmail form2Email;
        public AsyncProcessBehavior asyncBehavior;
        public List<PdfUserClass> businessList = new List<PdfUserClass>();
        public bool SelectedAll = true;
        public static Form1 instance;

        public Form1()
        {
            instance = this;
            InitializeComponent();
        }

        private void ShowMessageBox(string title, string message, Action confirmAction, Action cancelAction, MessageBoxButtons buttons)
        {
            DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                confirmAction.Invoke();
            }
            else if (cancelAction != null)
            {
                cancelAction.Invoke();
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            this.listView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.List_RightClick);

            listView1.View = View.Details;
            listView1.Sorting = SortOrder.Ascending;

            ColumnHeader columnHeader0 = new ColumnHeader();
            columnHeader0.Text = "Nome";
            columnHeader0.Width = 250;

            // Add the column headers to myListView.
            listView1.Columns.AddRange(new ColumnHeader[]
                {columnHeader0});
            
            asyncBehavior = new AsyncProcessBehavior(this);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            List<string> emailsToSend = new List<string>();
            foreach (ListViewItem item in listView1.CheckedItems)
            { 
              emailsToSend.Add(item.Text);
            }

            string peopleList = "\n";
            int i = 0;
            foreach (string str in emailsToSend)
            {
                peopleList += str + "\n";
                i++;
                if (i > 10)
                {
                    peopleList += "...";
                    break;
                }
            }

            ShowMessageBox("Sucesso", "Emails para " + peopleList,
                () => {
                    asyncBehavior.SendEmailTo(emailsToSend.ToArray());
                }, null, MessageBoxButtons.OK);
        }

        internal void FillForm(Settings settings)
        {
            smtpServer.Text  = settings.SMTPServer;
            smtpPort.Text = settings.SMTPPort + "";
            smtpEmail.Text = settings.SMTPUser;
            smtpPassword.Text = settings.SMTPPassword;
            assuntoEmail.Text = settings.EmailSubject;
            messageBoxEmail.Lines = settings.MessagePattern;
        }

        public void AddToChecklistBox(string str, string path)
        {
            PdfUserClass businessValueClass = new PdfUserClass(str, path);
            businessList.Add(businessValueClass);
            ListViewItem newItem = new ListViewItem(new string[]
                {businessValueClass.Nome});
            newItem.Tag = businessValueClass;
            listView1.Items.Add(newItem);
            businessValueClass.UpdateInformation();
            log("Novo Contato para envio de emails Adicionado");
            log(businessValueClass.Nome);
            log(" ");
        }

        internal void SetSuccessEmail(PdfUserClass value)
        {
            try
            {
                int i = BusinessPositionOnList(value);
                listView1.Items[i].BackColor = Color.White;
            }
            catch (Exception e)
            {
                return;
            }

        }

        public void ProgressCounter(int i)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action(() =>
                {
                    progressBar1.Value = i;
                }));
                return;
            }
            progressBar1.Value = i;
        }

        public void Loading(bool b)
        {
            this.Invoke(new Action(() => {
                button1.Enabled = b;
                button2.Enabled = b;
                button3.Enabled = b;
            }));
        }

        internal void AdicionaFila(string v)
        {
            string substring = v.Substring(v.LastIndexOf("\\")+1);
            this.Invoke(new Action(() => { listView2.Items.Add(v, substring,1); }));
        }


        internal void RemoveFila(string v)
        {
            this.Invoke(new Action(() => {
                listView2.Items.RemoveByKey(v);
            }));
        }

        public void UnlockInterface()
        {
            this.Invoke(new Action(() => { button2.Enabled = true; }));
        }

        public void SuccessEmail(PdfUserClass value)
        {
            int i = businessList.IndexOf(value);
        }

        public void SetFailEmail(PdfUserClass value)
        {
            try
            {
                int i = BusinessPositionOnList(value);
                listView1.Items[i].BackColor = Color.Red;
            }
            catch (Exception e)
            {
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "-- Inicio do Programa --";
            openFileDialog1.Filter = "zip files (*.zip)|*.zip|All files (*.*)|*.*";
            businessList.Clear();
            listView1.Items.Clear();
            button2.Enabled = false;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (openFileDialog1.FileName.Contains(".zip"))
                {
                    log("Zip Descompactado em " + openFileDialog1.FileName);
                    log("Lendo PDFs favor Aguarde... ");
                    asyncBehavior.ProcessZio(openFileDialog1.FileName);
                }
                else if(openFileDialog1.FileName.Contains(".pdf"))
                {
                    log("Lendo PDF favor Aguarde... ");
                    asyncBehavior.ProcessPDF(openFileDialog1.FileName);
                }
                else
                {
                    log("Arquivo Invalido favor selecione um .zip ou .pdf");
                    log("");
                }
            };
        }


        private void List_RightClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    if (listView1.FocusedItem.Bounds.Contains(e.Location) == true)
                    {
                        PdfUserClass personMouseOver = FindPersonSameName((PdfUserClass)listView1.FocusedItem.Tag);
                        if (personMouseOver != null)
                        {
                            form2Email = new SingleEmail(personMouseOver);
                            form2Email.Show();
                        }
                    }
                }
            }
            catch (Exception)
            {
                return;
            }

        }

        public PdfUserClass FindPersonSameName(PdfUserClass bcValueClass)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Tag.Equals(bcValueClass))
                {
                    return (PdfUserClass)item.Tag;
                }
            }

            return null;
        }

        public PdfUserClass FindPersonSameName(string bcValueClass)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Text.Contains(bcValueClass))
                {
                    return (PdfUserClass)item.Tag;
                }
            }

            return null;
        }

        private int[] FindAllPositions(string Name)
        {
            List<int> tempList = new List<int>();
            foreach (PdfUserClass bvc in businessList)
            {
                tempList.Add(BusinessPositionOnList(bvc));
            }

            return tempList.ToArray();
        }

        private int BusinessPositionOnList(PdfUserClass bvc)
        {
            int i = 0;
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Tag.Equals(bvc))
                {
                    return i;
                }

                i++;
            }

            i = 0;
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Text.Contains(bvc.Nome))
                {
                    return i;
                }

                i++;
            }

            return 0;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.Enabled = false;
            Settings settings = new Settings();
            settings.SMTPServer = smtpServer.Text;
            int.TryParse(smtpPort.Text, out settings.SMTPPort);
            settings.SMTPUser = smtpEmail.Text;
            settings.SMTPPassword = smtpPassword.Text;
            settings.EmailSubject = assuntoEmail.Text;
            settings.MessagePattern = messageBoxEmail.Lines;
            
            ShowMessageBox("Sucesso", "Configuracoes Salvadas com Sucesso", 
                () => {
                    Settings.WriteGarageXML(settings);
                    button3.Enabled = true;
                }, null, MessageBoxButtons.OK);
        }

        public void log(string str)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action(() =>
                {
                    textBox1.Text += str + "\r\n";
                    textBox1.ScrollToCaret();
                }));
                return;
            }
            textBox1.Text += str + "\r\n";
            textBox1.ScrollToCaret();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://docs.google.com/spreadsheets/d/15TcCqOuDoJqIFzhzw3abWShogxadnB_bTLWve4KKnfY/edit?usp=sharing");        
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                item.Checked = SelectedAll;
            }

            SelectedAll = !SelectedAll;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Visible)
            {
                textBox1.SelectionStart = textBox1.TextLength;
                textBox1.ScrollToCaret();
            }
        }
    }
}
